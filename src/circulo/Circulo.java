/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package circulo;
import java.util.Scanner;
/**
 *
 * @author HP 14
 */
public class Circulo {

    float x;
    float y;
    float r;
    
    Circulo(float x, float y){
        this.x=x;
        this.y=y;
    }
    
    public Circulo(float x, float y, float r){
        this.x=x;
        this.y=y;
        this.r=r;
              
    }
    public Circulo (float r){
        this.r=r;
    }
    public void setX(float x){
        this.x=x;
    }
    public void setY(float y){
        this.y=y;
    }
    public void setR(float r){
        this.r=r;
    }
    
    float getX(){
        return(this.x);
    }
    
    float getY(){
        return(this.y);
    }
    float getR(){
        return(this.r);
    }
    
    void showEcuacionOrdinaria(){
       
        System.out.printf("(x-%.2f)^2+(y-%.2f)^2=%.2f^2",getX(),getY(),getR());
    }
    
    void showValorDelArea(){
        System.out.printf("%.3f",Math.PI*getR()*getR());
    }
    
    void showPerimetro(){
        System.out.printf("%.3f",2*Math.PI*getR());
    }
    
    void showRadioSabiendoDiametro(){
        System.out.printf("%.3f",getR()/2);
    }
    
  
    /**
     * @param args the command line arguments1
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**Circulo c1=new Circulo(1,1,1);
            c1.showEcuacionOrdinaria();
            */
            float x,y,r;
            int op=0;
            Circulo c1, c2, c3, c4;
            Scanner t=new Scanner(System.in);
            /*System.out.print("Dame el valor de x: ");
            x=t.nextFloat();
            System.out.print("Dame el valor de y: ");
            y=t.nextFloat();
            System.out.print("Dame el valor de radio: ");
            r=t.nextFloat();
            c1=new Circulo(x,y,r);
            c1.showEcuacionOrdinaria();
            
            System.out.println("Dame el valor del radio: ");
            r=t.nextFloat();
            c2=new Circulo(x,y,r);
            c2.showValorDelArea();
            
            System.out.print("Dame el valor del radio: ");
            r=t.nextFloat();
            c3=new Circulo(x,y,r);
            c3.showPerimetro();*/
            System.out.print("Bienvenido\n"); 
            System.out.println("¿Qué quieres hacer?\n 1) Encontrar la escuación ordinaria\n "
                + "2) Calcular el valor de área\n "
                + "3) Calcular el perímetro\n"
                + "4) Calcular elradio sabiendo el diámetro");
        
        while(op!=3){//Abre while
            
        op=t.nextInt();
        switch(op){
            
            case 1://Encontrar la ecuación ordinaria
            System.out.print("Dame el valor de x: ");
            x=t.nextFloat();
            System.out.print("Dame el valor de y: ");
            y=t.nextFloat();
            System.out.print("Dame el valor de radio: ");
            r=t.nextFloat();
            System.out.println("La Ecuación Ordinaria Del círculo es:\n");
            c1=new Circulo(x,y,r);
            c1.showEcuacionOrdinaria();
            System.out.print("¿Qué más quieres hacer?\n");
                
                 break ;
            
            case 2://Calcular el valor de área
             System.out.println("Dame el valor del radio: ");
            r=t.nextFloat();
            System.out.println("El área del círculo es:\n");
            c2=new Circulo(r);
            c2.showValorDelArea();
            System.out.print("¿Qué más quieres hacer?\n");
        
                 break ;
            case 3: //Calcular el perímetro
            System.out.print("Dame el valor del radio: ");
            r=t.nextFloat();
            System.out.println("El perímetro del círculo es:\n");
            c3=new Circulo(r);
            c3.showPerimetro();
            System.out.print("¿Qué más quieres hacer?\n");
            
                break;
            case 4://Calcular el radio sabiendo el diametro
            System.out.print("Dame el valor del diametro: ");
            r=t.nextFloat();
            System.out.print("El valor del radio es:\n");
            c4=new Circulo(r);
            c4.showRadioSabiendoDiametro();
            System.out.print("¿Qué más quieres?\n");
            
                break;
                }
            }
            }
     }
     
      

        